/*! This library gathers helper functions and macros I find useful. */

/** Declares a trait with some methods, and implements it for some types.

# Example

```
#[macro_use]
extern crate uflib ;

extend!{
  doc="Has a print function."
  pub trait HasPrint for usize, u32, u64, i32 {
    doc="Print function."
    fn print(& self) {
      println!("my value is `{}`", self)
    }
    doc="Another print function."
    fn print_2(& self) {
      println!("my value is `{}`", self)
    }
  }
}

fn main() {
  7.print() ;
  9u32.print_2()
}
```
*/
#[macro_export]
macro_rules! extend {
  (
    $( $met:meta ),* $(,)*
    pub trait $treit:ident for $( $typs:ty ),+ {$( $funs:tt)+}
  ) => (
    #[ $($met),+ ]
    pub trait $treit {
      extend!{ signatures of $($funs)+ }
    }
    extend!{ implems of $treit ($($typs),+) $($funs)+ }
  ) ;
  (
    $( $met:meta ),* $(,)*
    trait $treit:ident for $( $typs:ty ),+ {$( $funs:tt)+}
  ) => (
    #[ $($met),+ ]
    trait $treit {
      extend!{ signatures of $($funs)+ }
    }
    extend!{ implems of $treit ($($typs),+) $($funs)+ }
  ) ;

  (
    signatures of
      $($met:meta),+ $(,)*
      fn $fn_name:ident( $($arg:tt)*) $body:block
      $( $tail:tt )*
  ) => (
    #[ $($met),+ ]
    fn $fn_name( $($arg)* ) ;
    extend!{ signatures of $($tail)* }
  ) ;
  (
    signatures of
      $($met:meta),+ $(,)*
      fn $fn_name:ident( $($arg:tt)* ) -> $ret_t:ty $body:block
      $( $tail:tt )*
  ) => (
    #[$($met),+]
    fn $fn_name( $($arg)* ) -> $ret_t ;
    extend!{ signatures of $($tail)* }
  ) ;
  (signatures of) => () ;

  (
    funs of
      $($met:meta),+ $(,)*
      fn $fn_name:ident( $($arg:tt)*) $body:block
      $( $tail:tt )*
  ) => (
    #[ $($met),+ ]
    fn $fn_name( $($arg)* ) $body
    extend!{ funs of $($tail)* }
  ) ;
  (
    funs of
      $($met:meta),+ $(,)*
      fn $fn_name:ident( $($arg:tt)* ) -> $ret_t:ty $body:block
      $( $tail:tt )*
  ) => (
    #[$($met),+]
    fn $fn_name( $($arg)* ) -> $ret_t $body
    extend!{ funs of $($tail)* }
  ) ;
  (funs of) => () ;

  (
    implems of $treit:ident ( $typ:ty $(, $tail:ty )* ) $($funs:tt)+
  ) => (
    impl $treit for $typ { extend!{funs of $($funs)+} }
    extend!{ implems of $treit($($tail),*) $($funs)+ }
  ) ;
  (implems of $treit:ident () $($funs:tt)+) => () ;
}